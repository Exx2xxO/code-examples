package modules.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import modules.configuration.parameter.Parameter;
import modules.configuration.parameter.SourceParam;
import modules.configuration.sources.ConfigSource;
import modules.configuration.sources.SqlLiteSource;
import modules.configuration.utils.Dictionary;

public class ConfigSourceSync {

	private List<ConfigSource> sources;
	private Map<String, SourceParam> primaryParams;
	private int pIndex;
	
	public ConfigSourceSync() {
		this.sources = new ArrayList<>();
		this.sources.add(new SqlLiteSource("jdbc:sqlite:/path/or/url/to/database", "dbname", "dbuser", "dbpass"));
		this.pIndex = 0;
		this.primaryParams = this.sources.get(this.pIndex).readAll();
	}
	
	private boolean compareParamMap(Map<String, SourceParam> second) {
		if(second == null) return false;
		
		if(primaryParams.size() != second.size()) {
			return false;
		}
		
		for(String key : second.keySet()) {
			if(!this.primaryParams.containsKey(key)) {
				
				return false;
			} else {
				String v1 = this.primaryParams.get(key).value;
				String v2 = second.get(key).value;
				
				if(!v1.equalsIgnoreCase(v2)) return false;
			}
		}
		
		return true;
	}
	
	public Map<String, SourceParam> getAll() {
		final Map<String, SourceParam> m = this.sources.get(this.pIndex).readAll();
		return m; 
	}
	
	public void sync() {
		Map<String, SourceParam> secondParams;
		for(int i = 0; i < sources.size(); i++) {
			if(i != pIndex) {
				secondParams = sources.get(i).readAll();
				if(!this.compareParamMap(secondParams)) {
					this.sources.get(i).updateAll(this.primaryParams);
					return;
				}
			}
		}
	}
	
	public void addSource(ConfigSource src) {
		this.sources.add(src);
		if(!this.compareParamMap(src.readAll())) {
			
		}
	}
	
	public void setPrimary(int index) {
		this.pIndex = (index < sources.size()) ? index : this.pIndex;
		this.primaryParams = this.sources.get(this.pIndex).readAll();
	}
	
	public void removeSource(int index) {
		this.removeSource(index, -1);
	}
	
	public void removeSource(int index, int primary) {
		if(index < 0 || index >= this.sources.size()) return;
		
		if(this.sources.size() <= 1) {
			this.pIndex = 0; 
			return;
		}
		
		if(primary < 0 || primary > sources.size() || primary == index) primary = 0;
		
		this.pIndex = primary;
		this.sources.remove(index);
	}
	
	public void validateSource(ConfigCore core) {
		List<String> all = new ArrayList<>();
		all.addAll(Dictionary.getVarList(Dictionary.GeneralKey.class));
		all.addAll(Dictionary.getVarList(Dictionary.SerialKey.class));
		all.addAll(Dictionary.getVarList(Dictionary.GpioKey.class));
		
		for(String s : all) {
			if(!this.primaryParams.containsKey(s)) {
				Parameter<?> p = core.get(s);
				this.primaryParams.put(s, new SourceParam(p.getName(), (String) p.getValue(), p.getCategory()));
			}
		}
	}
}