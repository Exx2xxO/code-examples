package modules.configuration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import modules.configuration.parameter.DefaultParameterFactory;
import modules.configuration.parameter.Parameter;
import modules.configuration.parameter.ParameterFactory;
import modules.configuration.parameter.SourceParam;

public class ConfigCore {

	private List<Parameter<?>> parameters;
	
	//private Map<ParameterFactory.Type, List<Integer>> paramTypeIndex;
	// private Map<String, List<Integer>> paramCatIndex;
	
	private Map<String, Integer> paramLookUp;
	
	private AtomicBoolean changed;
	private ConfigSourceSync sync;
	
	public ConfigCore() {
		this.parameters = DefaultParameterFactory.getInstance().getDefaults();
		this.paramLookUp = new HashMap<>();

		for(int index = 0; index < this.parameters.size(); index++) {
			Parameter<?> p = this.parameters.get(index);
			this.paramLookUp.put(p.getName(), index);
		}

		this.sync = new ConfigSourceSync();
		this.sync.validateSource(this);
	}
	
	public Parameter<?> get(String name) {
		if(this.paramLookUp.containsKey(name)) {
			return this.parameters.get(this.paramLookUp.get(name));
		}
		
		return null;
	}
	
	public <T> void set(String name, T value) {
		if(this.paramLookUp.containsKey(name)) {
			this.parameters.get(this.paramLookUp.get(name)).setValue(value);
			this.changed.compareAndSet(false, true);
		}
	}
	
	public <T> void set(String name, String cat, T value, List<Parameter<T>> params) {
		for(Parameter<T> p : params) {
			if(p.getName().equalsIgnoreCase(name) && p.getCategory().equalsIgnoreCase(cat)) {
				p.setValue(value);
				this.changed.compareAndSet(false, true);
			}
		}
	}
	
	public void readSources() {
		ParameterFactory f = ParameterFactory.getInstance();
		Map<String, SourceParam> params = this.sync.getAll();
		Map<String, Parameter<?>> paramMap = new HashMap<>();
		for(String name : params.keySet()) {
			SourceParam src = params.get(name);
			paramMap.put(name, f.makeParam(src.name, src.value, src.category));
		}
		
		if(!this.compareParamMap(paramMap)) {
			this.compareAndSet(paramMap);
		}
	}
	
	private void compareAndSet(Map<String, Parameter<?>> update) {
		for(String name : this.paramLookUp.keySet()) {
			Object v1 = this.parameters.get(this.paramLookUp.get(name)).getValue();
			Object v2 = update.get(name).getValue();
			
			if(!v1.equals(v2)) {
				this.parameters.get(this.paramLookUp.get(name)).setValue(update.get(name).getValue());
			}
		}
	}
	
	public void queryUpdate() {
		if(this.changed.get() == true) {
		}
	}
	
	private boolean compareParamMap(Map<String, Parameter<?>> second) {
		if(second == null) return false;
		
		if(this.paramLookUp.size() != second.size()) {
			return false;
		}
		
		for(String key : second.keySet()) {
			if(!this.paramLookUp.containsKey(key)) {
				return false;
			} else {
				Object v1 = this.parameters.get(this.paramLookUp.get(key)).getValue();
				Object v2 = second.get(key).getValue();
				
				if(!v1.equals(v2)) return false;
			}
		}
		
		
		return true;
	}
}
