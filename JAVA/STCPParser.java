package modules.stcp;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Arrays;
import java.util.Calendar;

import com.main.config.Config;
import com.main.config.DefaultConfig;

import utils.Filetype;
import utils.Log;
import utils.RPiFileSystemControl;

public class STCPParser {

	private STCPModule	module;
	private byte[]		buffer;
	private int			writeMarker;

	private RPiProgramBuffer programBuffer;

	public STCPParser(STCPModule module) {
		this.module = module;
		this.buffer = new byte[1024];
		this.programBuffer = new RPiProgramBuffer(null, this.module);
		this.writeMarker = 0;
	}

	public void read(byte b) {
		if (b != Const.CARRIAGE_RETURN && b != Const.NEW_LINE && b > 31) {
			this.addToBuffer(b);
		} else if (b == Const.CARRIAGE_RETURN) {
			Const.FLAG_CR = true;
			this.parse();
		} else if (Const.FLAG_CR && b == Const.NEW_LINE) {
			Const.FLAG_CR = false;
		} else {
			Const.FLAG_CR = false;
			if (this.writeMarker > 0) {
				this.parse();
			}
		}
	}

	private void addToBuffer(byte b) {
		if (this.writeMarker < this.buffer.length) {
			this.buffer[this.writeMarker] = b;
		} else {
			this.buffer = Arrays.copyOf(this.buffer, this.buffer.length * 2);
			this.buffer[this.writeMarker] = b;
		}
		this.writeMarker++;
	}

	private void parse() {
		String line = new String(this.buffer, 0, this.writeMarker);
		int percentageIndex = line.indexOf('%');
		
		if(percentageIndex > -1) {
			line = line.substring(percentageIndex, line.length());
		}

		if (Const.FLAG_PRG && !line.isEmpty()) {
			this.programBuffer.saveProgram(line);
		}

		if (percentageIndex > -1 && !Const.FLAG_PRG) {

			if (this.programBuffer.timerDone(Instant.now())) {
				this.programBuffer.reset();
				this.programBuffer.createBuffer(null);
			}

			Const.FLAG_PRG = true;
			this.programBuffer.saveProgram(line);
		} else if (percentageIndex > -1 && Const.FLAG_PRG) {
			Const.FLAG_PRG = false;
			this.programBuffer.setTime();
		}

		if (line.contains(Const.BDE) && !Const.FLAG_PRG) {
			int indexBDE = line.indexOf(Const.BDE) + Const.BDE.length();
			if(indexBDE > line.length()) {
				return;
			} else {
				line = line.substring(indexBDE);
			}
			line = line.trim();
			this.saveBDE(line);
		}

		if (line.contains(Const.OFFSET) && !Const.FLAG_PRG) {
			int indexOffset = line.indexOf(Const.OFFSET) + Const.OFFSET.length();
			if(indexOffset > line.length()) {
				return;
			} else {
				line = line.substring(indexOffset);
			}
			line = line.trim();
			this.savePM(line);
		}

		this.restart();
	}

	private void savePM(String pmline) {
		String filePath = Config.get(DefaultConfig.DIRECTORY_PM_FILE);
		String fileName = "";
		
		int lastBS = filePath.lastIndexOf('/');
		if(lastBS == -1) {
			lastBS = filePath.lastIndexOf('\\');
			if(lastBS == -1) {
				Log.printLog(Log.WARN, "No valid path found. Saving to "+DefaultConfig.DIRECTORY_PM_FILE.getValue());
				filePath = DefaultConfig.DIRECTORY_PM_FILE.getValue();
			}
		}
		
		int lastDot = filePath.lastIndexOf('.');
		if(lastDot == -1 && lastDot > lastBS) {
			fileName = filePath.substring(lastBS + 1);
			filePath = filePath.substring(0, lastBS + 1);
		} else {
			fileName = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime()) + Filetype.CNC;
		}
		
		String lineHeader = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(Calendar.getInstance().getTime());
		String line = lineHeader + " " + Config.get(DefaultConfig.MACHINE_NUMBER) + " " + pmline + "\n";
		
		RPiFileSystemControl.instance().writeInFile(line.getBytes(), filePath, fileName);
	}

	private void saveBDE(String bdeline) {
		String lineHeader = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(Calendar.getInstance().getTime());
		String line = lineHeader + " " + Config.get(DefaultConfig.MACHINE_NUMBER) + " " + bdeline + "\n";

		this.module.tcpOutAll(line.getBytes());
	}

	private void restart() {
		this.buffer = new byte[1024];
		this.writeMarker = 0;

		Const.FLAG_CR = false;
	}

	public void reset() {
		this.buffer = new byte[1024];
		this.writeMarker = 0;
		this.programBuffer.reset();

		Const.FLAG_CR = false;
		Const.FLAG_PRG = false;
	}
}
