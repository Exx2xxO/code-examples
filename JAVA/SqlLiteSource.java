package modules.configuration.sources;

import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import modules.configuration.DriverShim;
import modules.configuration.parameter.SourceParam;

public class SqlLiteSource implements ConfigSource {

	private String		dbUrl;
	private String		tableName;
	private String		user;
	private char[]		pass;

	private Connection	conn;

	public SqlLiteSource(String databaseUrl, String databaseName, String user, String pass) {
		this.dbUrl = databaseUrl;
		this.user = user;
		this.pass = pass.toCharArray();
		this.tableName = databaseName;
	}

	private Connection makeConn() throws SQLException {
		try {
			URL u = new URL("jar:file:/bin/sqlitejdbc38.jar!/");
			String classname = "org.sqlite.JDBC";
			URLClassLoader ucl = new URLClassLoader(new URL[] { u });
			Driver d = (Driver) Class.forName(classname, true, ucl).newInstance();
			DriverManager.registerDriver(new DriverShim(d));
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (this.user != null && !this.user.isEmpty()) {
			return DriverManager.getConnection(this.dbUrl, this.user, new String(this.pass));
		} else {
			return DriverManager.getConnection(this.dbUrl);
		}
	}

	private void open() {
		try {
			if (this.conn == null || this.conn.isClosed()) this.conn = this.makeConn();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void execute(String sql) {
		this.open();
		try {
			Statement stmnt = this.conn.createStatement();
			stmnt.execute(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private ResultSet executeQuery(String sql) {
		this.open();
		ResultSet results = null;
		try {
			Statement stmnt = this.conn.createStatement();
			results = stmnt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return results;
	}

	private SourceParam resultToSrcParam(ResultSet result) {
		SourceParam p = null;
		try {
			if (result != null) {
				p = new SourceParam(result.getString("name"), result.getString("value"), result.getString("category"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return p;
	}

	@Override
	public void create(SourceParam param) {
		String query = "INSERT INTO " + this.tableName + " VALUES('" + param.name + "', '" + param.value + "', '"
				+ param.category + "');";
		this.execute(query);
	}

	@Override
	public SourceParam read(String name) {
		String query = "SELECT " + name + " FROM " + this.tableName + " WHERE name='" + name + "';";
		ResultSet result = this.executeQuery(query);
		try {
			if (result != null && result.next()) {
				return this.resultToSrcParam(result);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public List<SourceParam> readCategory(String category) {
		String query = "SELECT * FROM " + this.tableName + " WHERE category='" + category + "';";
		ResultSet result = this.executeQuery(query);
		List<SourceParam> readParams = new ArrayList<>();
		try {
			while (result.next()) {
				readParams.add(new SourceParam(result.getString("name"), result.getString("value"),
						result.getString("category")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return readParams;
	}

	@Override
	public Map<String, SourceParam> readAll() {
		String query = "SELECT * FROM " + this.tableName + ";";
		ResultSet result = this.executeQuery(query);
		Map<String, SourceParam> paramMap = new HashMap<>();
		try {
			while (result.next()) {
				paramMap.put(result.getString("name"), new SourceParam(result.getString("name"),
						result.getString("value"), result.getString("category")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
        
        /* Keine Freigabe, aus Gründen der Geheimhaltung wurde dieser Abschnitt entfernt */

		return null;
	}

	@Override
	public void update(SourceParam param) {
		String query = "UPDATE " + this.tableName + " value='" + param.value + "' WHERE name='" + param.name + "';";
		this.execute(query);
	}

	@Override
	public void updateCategory(List<SourceParam> params) {
		for (SourceParam p : params) {
			this.update(p);
		}
	}

	@Override
	public void updateAll(Map<String, SourceParam> paramMap) {
		for (SourceParam p : paramMap.values()) {
			this.update(p);
		}
	}

	@Override
	public void delete(String name) {
		String query = "DELETE FROM " + this.tableName + " WHERE name='" + name + "'";
		this.execute(query);
	}

	@Override
	public void delete(String name, String category) {
		String query = "DELETE FROM " + this.tableName + " WHERE name='" + name + "' AND category='" + category + "';";
		this.execute(query);
	}

}
