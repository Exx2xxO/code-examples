package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.main.config.Config;
import com.main.config.DefaultConfig;

import interfaces.RPiFileSystem;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFileOutputStream;

public class RPiFileSystemControl implements RPiFileSystem {

	private List<OutputStream> openStreams;

	private static RPiFileSystemControl instance;

	private RPiFileSystemControl() {
		this.openStreams = new ArrayList<>();
	}

	public static RPiFileSystemControl instance() {
		if (instance == null) {
			instance = new RPiFileSystemControl();
		}

		return instance;
	}

	private boolean writeInternal(byte[] data, String path, String name, boolean append) {
		File f = Paths.get(path, name).toFile();

		if (!f.exists()) {
			this.createFile(path, name);
		}

		if (f.exists()) {

			FileOutputStream out;

			try {
				if (append) {
					out = new FileOutputStream(Paths.get(path, name).toFile(), true);
				} else {
					out = new FileOutputStream(Paths.get(path, name).toFile(), false);
				}

				out.write(data, 0, data.length);
				out.flush();
				out.close();
				return true;
			} catch (FileNotFoundException e) {
				Log.printLog(Log.WARN, "File " + path + "/" + name + " not found.");
			} catch (IOException e) {
				Log.printLog(Log.ERR, "An error occured while writing file " + path + "/" + name
						+ ". See log files for more information.");
				Log.silentLog(e);
			}
		} else {
			return false;
		}

		return false;
	}

	private boolean writeSmbInternal(byte[] data, String path, String name, boolean append) {
		if (!path.startsWith("smb:")) {
			path = "smb:" + path;
		}

		path = path.replace('\\', '/');

		NtlmPasswordAuthentication auth = this.makeAuth();

		try {
			SmbFile file = new SmbFile(path + name, auth);

			if (!file.exists()) {
				this.createSmbFile(path, name);
			}

			SmbFileOutputStream out;

			if (append) {
				out = new SmbFileOutputStream(file, true);
			} else {
				out = new SmbFileOutputStream(file, false);
			}

			out.write(data, 0, data.length);
			out.flush();
			out.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public boolean writeInFile(byte[] data, String path, String name) {
		if (path.startsWith("\\\\") || path.startsWith("//") || path.startsWith("smb")) {
			return this.writeInSmb(data, path, name);
		}

		return this.writeInternal(data, path, name, true);
	}

	@Override
	public boolean writeFile(byte[] data, String path, String name) {
		if (path.startsWith("\\\\") || path.startsWith("//") || path.startsWith("smb")) {
			return this.writeSmb(data, path, name);
		}

		return this.writeInternal(data, path, name, false);
	}

	@Override
	public boolean writeInSmb(byte[] data, String path, String name) {
		return this.writeSmbInternal(data, path, name, true);
	}

	@Override
	public boolean writeSmb(byte[] data, String path, String name) {
		return this.writeSmbInternal(data, path, name, false);
	}

	@Override
	public int writeHoldOpenFile(byte[] data, String path, String name) {
		if (path.startsWith("\\\\") || path.startsWith("//") || path.startsWith("smb") || path.startsWith("SMB")) {
			return this.writeHoldOpenSmb(data, path, name);
		}

		File f = Paths.get(path, name).toFile();
		int id = -1;

		if (!f.exists()) {
			this.createFile(path, name);
		}

		if (f.exists()) {

			FileOutputStream out;

			try {
				out = new FileOutputStream(f, true);
				out.write(data, 0, data.length);
				this.openStreams.add(out);
				id = this.openStreams.size() - 1;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		return id;
	}

	@Override
	public int writeHoldOpenSmb(byte[] data, String path, String name) {
		if (!path.startsWith("smb:")) {
			path = "smb:" + path;
		}

		path = path.replace('\\', '/');

		NtlmPasswordAuthentication auth = this.makeAuth();

		int id = -1;
		try {
			SmbFile f = new SmbFile(path + "/" + name, auth);
			SmbFileOutputStream out = new SmbFileOutputStream(f, true);
			out.write(data, 0, data.length);
			this.openStreams.add(out);
			id = this.openStreams.size() - 1;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (SmbException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return id;
	}

	@Override
	public boolean writeToOpenFile(int streamId, byte[] data) {
		try {
			this.openStreams.get(streamId).write(data, 0, data.length);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public boolean createFile(String path, String name) {
		if (path.startsWith("\\\\") || path.startsWith("//") || path.startsWith("smb")) {
			return this.createSmbFile(path, name);
		}

		File f = Paths.get(path, name).toFile();
		File parent = f.getParentFile();

		try {
			if (f.exists()) {
				return true;
			} else if (parent != null && parent.exists() && parent.isDirectory()) {
				if (f.createNewFile()) {
					return true;
				}
			} else if (parent != null && parent.mkdirs()) {
				if (f.createNewFile()) {
					return true;
				}
			} else {
				if (f.mkdirs()) {
					if (f.createNewFile()) {
						return true;
					}
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean createSmbFile(String path, String name) {
		if (!path.startsWith("smb:")) {
			path = "smb:" + path;
		}

		if (path.contains("\\")) {
			path = path.replace('\\', '/');
		}

		NtlmPasswordAuthentication auth = this.makeAuth();

		try {
			SmbFile f = new SmbFile(path, auth);

			if (!f.exists()) {
				f.mkdirs();

				f = new SmbFile(path + "/" + name, auth);
				f.createNewFile();
			}
		} catch (MalformedURLException e) {
			Log.printLog(Log.ERR, "An error occured while creating file " + path + "/" + name
					+ ". See log files for more information.");
			Log.silentLog(e);
		} catch (SmbException e) {
			Log.printLog(Log.ERR, "An error occured while creating file " + path + "/" + name
					+ ". See log files for more information.");
			Log.silentLog(e);
		}

		return false;
	}

	public List<String> readSMBFile(String path, String name) {
		if (!path.startsWith("smb:")) {
			path = "smb:" + path;
		}

		if (path.contains("\\")) {
			path = path.replace('\\', '/');
		}

		NtlmPasswordAuthentication auth = this.makeAuth();

		try {
			SmbFile f = new SmbFile(path + "/" + name, auth);
			if (!f.canRead() || !f.isFile()) {
				return null;
			}

			BufferedReader in = null;
			SmbFileInputStream fis = null;
			List<String> lines = new ArrayList<>();
			try {
				fis = new SmbFileInputStream(f);
				in = new BufferedReader(new InputStreamReader(fis));
				String line = null;
				while ((line = in.readLine()) != null) {
					lines.add(line);
				}

				return lines;
			} catch (IOException e) {
				Log.printLog(Log.ERR,
						"An error occured while reading file " + f.getPath() + ". See log files for more information.");
				Log.silentLog(e);
			} finally {
				if (in != null) {
					try {
						fis.close();
						in.close();
					} catch (IOException e) {
					}
				}
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (SmbException e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<String> readFile(String filePath) {
		int indexBS = filePath.lastIndexOf('/');
		if (indexBS == -1) {
			indexBS = filePath.lastIndexOf('\\');
			if (indexBS == -1) {
				return null;
			}
		}

		String path = filePath.substring(0, indexBS);

		int indexDot = filePath.lastIndexOf('.');
		if (indexDot == -1) {
			return null;
		}

		String fileName = filePath.substring(indexBS + 1);
		return this.readFile(path, fileName);
	}

	@Override
	public List<String> readFile(String path, String name) {
		if (path.startsWith("//") || path.startsWith("\\\\") || path.startsWith("smb")) {
			return this.readSMBFile(path, name);
		}

		File f = new File(path + "/" + name);

		if (!f.canRead() || !f.isFile()) {
			return null;
		}

		BufferedReader in = null;
		List<String> lines = new ArrayList<>();
		try {
			in = new BufferedReader(new FileReader(f));
			String line = null;
			while ((line = in.readLine()) != null) {
				lines.add(line + "\n");
			}

			return lines;
		} catch (IOException e) {
			Log.printLog(Log.ERR, "An error occured while reading the file " + path + "/" + name
					+ ". See log files for more information.\n");
			Log.silentLog(e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
				}
			}
		}

		return null;
	}

	@Override
	public List<String> readFile(Path path) {
		try {
			List<String> fileLines = new ArrayList<>();

			fileLines = Files.lines(path).map(s -> s.trim()).filter(s -> !s.isEmpty())
					.collect(Collectors.toList());

			return fileLines;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public void reset() {
		if (!this.openStreams.isEmpty()) {
			for (OutputStream s : this.openStreams) {
				try {
					s.close();
				} catch (IOException e) {
					Log.printLog(Log.ERR, "A fatal error in the 'RPiFileSystemControl' occured.\n");
					Log.silentLog(e);
				}
			}
		}
	}

	@Override
	public void closeFile(int id, byte... data) {
		if (data.length > 0) {
			OutputStream out = this.openStreams.remove(id);
			try {
				out.write(data, 0, data.length);
				out.flush();
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void removeFile(String filePath, String fileName) {
		File f = Paths.get(filePath, fileName).toFile();

		if (f.exists()) {
			f.delete();
		}
	}

	public NtlmPasswordAuthentication makeAuth() {
		System.setProperty("jcifs.smb.client.domain", "hoehne");
		String name = Config.get(DefaultConfig.SMB_LOGIN_NAME);
		String pass = Config.get(DefaultConfig.SMB_LOGIN_PWD);

		if ((name != null && pass != null) && (!name.isEmpty() && !pass.isEmpty())) {
			return new NtlmPasswordAuthentication(null, name, pass);
		} else if (name != null && !name.isEmpty()) {
			return new NtlmPasswordAuthentication(null, name, null);
		} else {
			return new NtlmPasswordAuthentication(null, null, null);
		}
	}

	public NtlmPasswordAuthentication makeAuth(String path) {
		if (path.startsWith("smb")) {
			path = path.substring(6);
			int nextBS = path.indexOf('/');
			if (nextBS == -1) {
				nextBS = path.indexOf('\\');
				if (nextBS == -1) {
					path = "";
				}
			} else {
				path = path.substring(0, nextBS);
			}
		} else {
			path = path.substring(2);
			int nextBS = path.indexOf('/');
			if (nextBS == -1) {
				nextBS = path.indexOf('\\');
				if (nextBS == -1) {
					path = "";
				}
			} else {
				path = path.substring(0, nextBS);
			}
		}

		String[] names = Config.get(DefaultConfig.SMB_LOGIN_NAME).split(";");
		String name = "";
		if (names.length > 0) {
			for (String s : names) {
				if (s.contains(path)) {
					int indexOfAt = s.indexOf('@');
					name = s.substring(0, indexOfAt);
					break;
				}
			}
		} else {
			name = names[0];
		}
		String pass = Config.get(DefaultConfig.SMB_LOGIN_PWD);

		if ((name != null && pass != null) && (!name.isEmpty() && !pass.isEmpty())) {
			return new NtlmPasswordAuthentication(null, name, pass);
		} else if (name != null && !name.isEmpty()) {
			return new NtlmPasswordAuthentication(null, name, null);
		} else {
			return new NtlmPasswordAuthentication(null, null, null);
		}
	}
}
