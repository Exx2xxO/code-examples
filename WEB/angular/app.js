// Module

var calculator = angular.module('hbRechner', ['ngRoute']);

// Route 

calculator.config(function($routeProvider){
    $routeProvider
    .when('/', {
        templateUrl : 'pages/home.htm',
        controller : 'homeCtrl' 
    });
})

// Controller
calculator.controller('homeCtrl', ['$scope', function($scope){
    
    // Haustypen mit dem jeweiligen Isolations-Index
    
    $scope.buildingType = [
        {
            name : 'Passivhaus/Effizienzhaus', 
            isolations : [{
                name : 'Gut',
                value : '1'
            }]
        }, 
        {
            name :'Niedrigenergiehaus', 
            isolations : [{
                name : 'Gut',
                value : '2'
            }]
        },
        {
            name : 'Altbau', 
            isolations : [{
                name : 'Gut',
                value : '3'
            }, {
                name : 'Mittlemäßig',
                value : '4',
            }, {
                name : 'Schlecht',
                value : '5'
            },{
                name : 'Keine',
                value : '6'
            }]
        },
        {
            name : 'Neubau', 
            isolations : [{
                name : 'Gut',
                value : '7',
            },{
                name : 'Mittelmäßig',
                value : '8'
            },{
                name : 'Schlecht',
                value : '9'
            }]
        }
    ];
    
    // Raumtypen mit entsprechenden Isolationswerten
    
    $scope.roomType = [
        {
            name : 'Wohnraum',
            values : [12,19,28,30,35,38,24,26,28]
        },
        {
            name : 'Bad/WC Teilfliesung',
            values : [30,30,35,35,38,38,35,35,35]
        },
        {
            name : 'Bad/WC Vollfliesung',
            values : [35,35,40,40,45,45,40,40,40]
        },
        {
            name : 'Schlafraum',
            values : [10,17,26,28,33,35,22,24,26]
        },
        {
            name : 'Kinderzimmer',
            values : [15,19,29,31,36,39,25,27,29]
        },
        { 
            name : 'Küche',
            values : [10,17,26,28,33,35,22,24,26]
        },
        {
            name : 'Offene Küche',
            values : [12,19,28,30,35,38,24,26,28]
        },
        {
            name : 'Dachgeschosszimmer',
            values : [15,20,30,32,37,40,26,28,30]
        },
        {
            name : 'Wintergarten',
            values : [40,40,40,45,45,45,40,45,45]
        },
        {
            name : 'Kellerraum',
            values : [15,20,30,32,37,40,26,28,30]
        },
    ];
    
    // (Noch nicht implementiert)
    
    $scope.usage = [
        'Zusatz- und Übergangsheizung',
        'Vollheizung'
    ];
    
    // Außenwände mit Multiplikationsfaktor (wobei mich noch der Faktor 1 irritiert, da dadurch ja keine Änderung eintritt?)
    
    $scope.outerWalls = [{
            cnt : 0,
            factor : 1
        },{
            cnt : 1,
            factor : 1
        },{
            cnt : 2,
            factor : 1.1
        },{
            cnt : 3,
            factor : 1.2
        },{
            cnt : 4,
            factor : 1.3
        }
    ];
    
    // Position mit Multiplikationsfaktor (noch nicht eingebunden)
    
    $scope.locations = [{
            name : '',
            factor : 1
        },{
            name : 'Keller und Boden warm',
            factor : 1
        },{
            name : 'Keller kalt',
            factor : 1.1
        },{
            name : 'Dach kalt',
            factor : 1.1
        },{
            name : 'Keller und Boden kalt',
            factor : 1.2
        }
    ];
    
    // Objekt mit allen vom User einzutragenden Werten
    
    $scope.chosen = {};
    $scope.chosen.buildingType = $scope.buildingType[2];
    $scope.chosen.isolation = $scope.chosen.buildingType.isolations[0];
    $scope.chosen.roomType = $scope.roomType[0];
    $scope.chosen.outerWalls = $scope.outerWalls[0];
    $scope.chosen.location = $scope.locations[0];
    $scope.chosen.length = 0;
    $scope.chosen.width = 0;
    $scope.chosen.height = 0;
    $scope.chosen.costs = 0;
    $scope.chosen.heatTime = 1;
    $scope.chosen.glassSurface = 0;
    $scope.result = 0;
    $scope.resultCosts = 0;
    
    // Helper Funktion zum anzeigen des derzeit ausgewählten Haustyps (übergangsweise muss bei implementierung des Designs überarbeitet werden)
    
    $scope.choseBuildClass = function(value) {
        if(value === $scope.chosen.buildingType.name) {
            return 'btn btn-primary'
        } else {
            return 'btn'
        }
    };
    
    // siehe choseBuildClass
    
    $scope.choseIsoClass = function(value) {
        if(value === $scope.chosen.isolation.name) {
            return 'btn btn-primary'
        } else {
            return 'btn'
        }
    };
    
    // Setzt den Wert für den gewählten Haustyp
    
    $scope.buildingClick = function(value) {
        $scope.chosen.buildingType = value;
        $scope.chosen.isolation = $scope.chosen.buildingType.isolations[0];
    }
    
    // Setzt den Wert für die gewählte Isolation
    
    $scope.isoClick = function(value) {
        $scope.chosen.isolation = value;
    }
    
    // Ruft bei Änderungen an den Werten die calculate() Funktion zum berechnen der neuen Ergebnisse auf
    
    $scope.$watchCollection('chosen', function(){
        $scope.calculate(); 
    });
    
    
    // Berechnet die Heizleistung und die monatlichen Kosten nach den vom User eingegebenen Werten
    
    $scope.calculate = function() {
        if($scope.chosen.length != 0 && $scope.chosen.width != 0 && $scope.chosen.height != 0) {
            $scope.result = (($scope.chosen.length * $scope.chosen.width * $scope.chosen.height) * $scope.chosen.outerWalls.factor + ($scope.chosen.glassSurface * 1.03)) * $scope.chosen.roomType.values[$scope.chosen.isolation.value - 1];
            
            $scope.result = $scope.result * $scope.chosen.heatTime;
        }
        
        if($scope.chosen.costs != 0 && $scope.result != 0 && $scope.chosen.heatTime != 0) {
            $scope.resultCosts = (($scope.chosen.costs * ($scope.result / 1000)) * $scope.chosen.heatTime) * 30.5; // kWh Preis für die erwartete Leistung * mit täglicher Heizzeit * für ein Monatspreis
        }
    }
}]);

